# NoSQL Database
**Created by Andy Banh for CS635 Object-Oriented Design and Programming**

This program was created as a practice exercise to learn different Object-Oriented Design patterns. The database can hold Integers, Doubles, Strings, Arrays, and Objects that are serializable (They do not need to inherit from the Serializable class). The data is stored in a text file in JSON format and is able to retain the classes of the object when retrieved from the text file through reflection.

This database features the following design patterns:

- Command
- Memento
- Chain of Responsibility
- Observer

The tests for this program were written using the JUnit Framework.

The assignment requirements can be found in this link:
http://www.eli.sdsu.edu/courses/fall22/cs635/assignments/Assignment3.pdf



