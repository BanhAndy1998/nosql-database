package cs635prog3;

/*
 * This class calls the remove method from a Database class
 * and removes the value from the database.
 */
public class RemoveCommand extends Command {

	public RemoveCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public Object execute() throws Exception {
		super.saveBackup(key);
		return db.remove(key);
	}
	
}
