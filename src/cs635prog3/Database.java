package cs635prog3;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Paths;

/*
 * This class is an implementation of a NOSQL Database.
 * Other classes interface with this class through the Command and Transaction classes.
 */

public class Database {
	private Map<String, Object> data;
	private Map<String, Cursor> cursors;
	private final File DEFAULT_SNAPSHOT_FILE;
	private final File DEFAULT_COMMANDS_FILE;
	
	public Database() {
		 data = new HashMap<String, Object>();
		 cursors = new HashMap<String, Cursor>();
		 DEFAULT_SNAPSHOT_FILE = new File("snapshot.txt");
		 DEFAULT_COMMANDS_FILE = new File("commands.txt");
	}	
	
	public Database put(String key, Object data) throws Exception {
		this.data.put(key, data);
		if(cursors.containsKey(key))
			cursors.get(key).update();
		return this;
	}
	
	public int getInt(String key) throws Exception {
		if(data.get(key) instanceof Integer)
			return (int) data.get(key);
		throw new Exception("Integer does not exist at this Key");
	}
	
	public double getDouble(String key) throws Exception {
		if(data.get(key) instanceof Double)
			return (double) data.get(key);
		throw new Exception("Double does not exist at this Key");
	}
	
	public String getString(String key) throws Exception {
		if(data.get(key) instanceof String)
			return (String) data.get(key);
		throw new Exception("String does not exist at this Key");
	}
	
	public DBArray getArray(String key) throws Exception {
		if(data.get(key) instanceof DBArray)
			return (DBArray) data.get(key);
		throw new Exception("Array does not exist at this Key");
	}
	
	public DBObject getObject(String key) throws Exception {
		if(data.get(key) instanceof DBObject)
			return (DBObject) data.get(key); 
		throw new Exception("Object does not exist at this Key");
	}
	
	public Object get(String key) throws Exception{
		if(data.containsKey(key))
			return (Object) data.get(key); 
		throw new Exception("Key has not been inserted into Database");
	}
	
	public Cursor getCursor(String key) {
		if(!cursors.containsKey(key))
			cursors.put(key, new Cursor(key,this));
		return cursors.get(key);
	}
	
	public Object remove(String key) throws Exception {
		if(cursors.containsKey(key))
			cursors.get(key).update();
		return data.remove(key);
	}
	
	public Snapshot snapshot() throws IOException {
		return new Snapshot(this, data, DEFAULT_COMMANDS_FILE, DEFAULT_SNAPSHOT_FILE);
	}
	
	/*
	 * Saves the database to the selected text files in JSON format.
	 */
	public Snapshot snapshot(File commands, File snapshot) throws IOException {
		return new Snapshot(this, data, commands, snapshot);
	}
	
	public Transaction restore() throws Exception {
		return restoreDatabase(DEFAULT_COMMANDS_FILE, DEFAULT_SNAPSHOT_FILE);
	}
	
	/*
	 * Restores the database from the selected text files.
	 */
	public Transaction restore(File commands, File snapshot) throws Exception {
		return restoreDatabase(commands, snapshot);
	}
	
	/*
	 * Restores the database to its last saved state. 
	 * Also converts ArrayLists to DBArrays, HashMaps to DBObjects, and BigDecimals to Doubles.
	 */
	private Transaction restoreDatabase(File commands, File snapshot) throws Exception {
		String retrievedString = Files.readAllLines(Paths.get(snapshot.getCanonicalPath())).get(0);
		JSONObject retrievedSnapshot = new JSONObject(retrievedString);
		Map<String, Object> retrievedMap = retrievedSnapshot.toMap();
		
		for(Entry<String, Object> retrievedData : retrievedMap.entrySet()) {
			if(retrievedData.getValue() instanceof BigDecimal) 
				retrievedMap.replace(retrievedData.getKey(), ((BigDecimal) retrievedData.getValue()).doubleValue());
			else if(retrievedData.getValue() instanceof java.util.ArrayList) 
				retrievedMap.replace(retrievedData.getKey(), DBArray.fromString(retrievedData.getValue().toString()));
			else if (retrievedData.getValue() instanceof java.util.HashMap) 
				retrievedMap.replace(retrievedData.getKey(), DBObject.convertNestedObjects(new DBObject((Map<String,Object>) retrievedData.getValue())));
		}
		
		this.data = retrievedMap;
		return restoreCommands(commands);
	}
	
	/*
	 * Restores any ongoing transactions that were not committed as a new transaction.
	 */
	private Transaction restoreCommands(File commands) throws Exception {
		Transaction restoredCommands = new Transaction(this);
		List<String> retrievedLines = Files.readAllLines(Paths.get(commands.getCanonicalPath()));
		
		for(String keyValue : retrievedLines) {
			JSONObject retrievedCommand = new JSONObject(keyValue);
			for(Entry<String, Object> retrievedData : retrievedCommand.toMap().entrySet()) {
				if(retrievedData.getValue() instanceof BigDecimal) 
					retrievedData.setValue(((BigDecimal) retrievedData.getValue()).doubleValue());
				else if(retrievedData.getValue() instanceof java.util.ArrayList) 
					retrievedData.setValue(DBArray.fromString(retrievedData.getValue().toString()));
				else if (retrievedData.getValue() instanceof java.util.HashMap) 
					retrievedData.setValue(DBObject.convertNestedObjects(new DBObject((Map<String,Object>) retrievedData.getValue())));
				try {
					restoredCommands.restoreFromLog(retrievedData.getKey(), retrievedData.getValue());
				} 
				catch (NoSuchMethodException | SecurityException | ClassNotFoundException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
			}
		}
		
		return restoredCommands;
	}
	
	public Transaction createTransaction() {
		return new Transaction(this);
	}
}
