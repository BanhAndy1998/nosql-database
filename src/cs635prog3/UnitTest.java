package cs635prog3;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class UnitTest {
	
	private final File DEFAULT_SNAPSHOT_FILE = new File("snapshot.txt");
	private final File DEFAULT_COMMANDS_FILE = new File("commands.txt");
	
	/*
	 * 
	 * DBArray Tests
	 * 
	 */
	
	@Test
	void DBArrayInsertRemoveTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(1);
		dba.remove(0);
		dba.insertEnd(2.0f);
		dba.remove(0);
		dba.insertEnd("ABC");
		dba.remove(0);
		DBArray storedDBArray = new DBArray();
		dba.insertEnd(new DBObject());
		dba.insertEnd(storedDBArray);
		dba.remove(0);
		assertEquals(storedDBArray, dba.get(0));
	}
	
	@Test
	void DBArrayInvalidInsertTest() {
		DBArray dba = new DBArray();
		dba.insertEnd(2.0f);
		dba.insertEnd(new Object());
		assertEquals(0, dba.length());
	}
	
	@Test
	void DBArrayGetValidStringTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd("Hello World!");
		dba.insertEnd("Message 1");
		try{
			assertEquals("Hello World!", dba.getString(0));
		}
		catch(Exception e ) {
			Assert.fail("Error: String was not found");
		}
	}
	
	@Test
	void DBArrayGetInvalidStringTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(24.02d);
		try{
			dba.getString(0);
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("String does not exist at this index or is out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayGetValidIntTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(3);
		try {
			assertEquals(3,dba.getInt(0));
		}
		catch(Exception e) {
			Assert.fail("Error: Integer was not found");
		}
	}
	
	@Test
	void DBArrayGetInvalidIntTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(2.0f);
		try {
			dba.getInt(0);
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Integer does not exist at this index or is out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayGetValidDoubleTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(3.9d);
		try {
			assertEquals(3.9d, dba.getDouble(0), 0.001);
		}
		catch(Exception e) {
			Assert.fail("Error: Integer was not found");
		}
	}
	
	@Test
	void DBArrayGetInvalidDoubleTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd("Hello");
		try {
			dba.getDouble(0);
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Double does not exist at this index or is out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayGetValidArrayTest() throws Exception {
		DBArray dba = new DBArray();
		DBArray dbb = new DBArray();
		dbb.insertEnd("Test Message");
		dba.insertEnd(dbb);
		try {
			assertEquals("Test Message", dba.getArray(0).getString(0));
		}
		catch(Exception e) {
			Assert.fail("Error: Array was not found");
		}
	}
	
	@Test
	void DBArrayGetInvalidArrayTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(2);
		try {
			dba.getArray(0);
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Array does not exist at this index or is out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayGetValidObjectTest() throws Exception {
		DBArray dba = new DBArray();
		DBObject dbo = new DBObject();
		dbo.put("Key","Test Message");
		dba.insertEnd(dbo);
		try {
			assertEquals("Test Message", dba.getObject(0).getString("Key"));
		}
		catch(Exception e) {
			Assert.fail("Error: Object was not found");
		}
	}
	
	@Test
	void DBArrayGetInvalidObjectTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(new DBArray());
		try {
			dba.getObject(0);
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Object does not exist at this index or is out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayValidGetTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd("Hello");
		dba.insertEnd(2.0d);
		dba.insertEnd(new DBArray());
		assertEquals(true , dba.get(1) instanceof Object);
	}
	
	@Test
	void DBArrayInvalidGetTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(2.0f);
		try{
			dba.get(0);
			Assert.fail("Error: Exception was not reached");
		}
		catch(Exception e) {
			assertEquals("Out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBArrayFromStringTest() throws Exception {
		try {
			DBArray dba = DBArray.fromString("[1, \"at\", [[1,2,3],3,4]]");
			assertEquals(true, dba.getArray(2).getArray(0) instanceof DBArray);
		}
		catch (Exception e) { 
			Assert.fail("Error: String was not converted to DBArray");
		}
	}
	
	@Test
	void DBArrayInvalidFromStringTest() throws Exception {
		try {
			DBArray dba = DBArray.fromString("1, \"at\", [[1,2,3],3,4]]");
			Assert.fail("Error: Exception was not reached when converting String to Array");
		}
		catch (Exception e) {
			assertEquals("Cannot be converted to JSONArray, invalid String", e.getMessage());
		}
	}
		
	/*
	 * 
	 * DBObject Tests
	 * 
	 */
	
	@Test
	void DBObjectPutRemoveTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key1", 1);
		dbo.remove("Key1");
		dbo.put("Key1", 4.0d);
		dbo.put("Key3", new DBObject());
		dbo.put("Key2", new DBArray());
		dbo.put("Key1", "Test");
		dbo.remove("Key2");
		assertEquals("Test", dbo.get("Key1"));
	}
	
	@Test
	void DBObjectInvalidPutTest() {
		DBObject dbo = new DBObject();
		dbo.put("Key",2.0f);
		try {
			dbo.get("Key");
			Assert.fail("Error: Exception should've been reached as float should not have been inserted");
		}
		catch (Exception e) {
			assertEquals("Key has not been inserted into this Object", e.getMessage());
		}
	}
	
	@Test
	void DBObjectGetValidStringTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key","Hello World!");
		try{
			assertEquals("Hello World!", dbo.getString("Key"));
		}
		catch(Exception e ) {
			Assert.fail("Error: String was not found");
		}
	}
	
	@Test
	void DBObjectGetInvalidStringTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", 24.02d);
		try{
			dbo.getString("Key");
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("String does not exist at this Key", e.getMessage());
		}
	}
	
	@Test
	void DBObjectGetValidIntTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", 3);
		try {
			assertEquals(3, dbo.getInt("Key"));
		}
		catch(Exception e) {
			Assert.fail("Error: Integer was not found");
		}
	}
	
	@Test
	void DBObjectGetInvalidIntTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", "Hello");
		try {
			dbo.getInt("Key");
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Integer does not exist at this Key", e.getMessage());
		}
	}
	
	@Test
	void DBObjectGetValidDoubleTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", 3.9d);
		try {
			assertEquals(3.9d, dbo.getDouble("Key"), 0.001);
		}
		catch(Exception e) {
			Assert.fail("Error: Double was not found");
		}
	}
	
	@Test
	void DBObjectGetInvalidDoubleTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", "Hello");
		try {
			dbo.getDouble("Key");
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Double does not exist at this Key", e.getMessage());
		}
	}
	
	@Test
	void DBObjectGetValidArrayTest() throws Exception {
		DBObject dbo = new DBObject();
		DBArray dba = new DBArray();
		dba.insertEnd("Test Message");
		dbo.put("Hello", dba);
		try {
			assertEquals("Test Message", dbo.getArray("Hello").getString(0));
		}
		catch(Exception e) {
			Assert.fail("Error: Array was not found");
		}
	}
	
	@Test
	void DBObjectGetInvalidArrayTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", 2);
		try {
			dbo.getArray("Key");
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Array does not exist at this Key", e.getMessage());
		}
	}
	
	@Test
	void DBObjectGetValidObjectTest() throws Exception {
		DBObject dbo = new DBObject();
		DBObject dbobj = new DBObject();
		dbobj.put("Key", "Test Message");
		dbo.put("Key", dbobj);
		try {
			assertEquals("Test Message", dbo.getObject("Key").getString("Key"));
		}
		catch(Exception e) {
			Assert.fail("Error: Object was not found");
		}
	}
	
	@Test
	void DBObjectGetInvalidObjectTest() throws Exception {
		DBObject dbo = new DBObject();
		dbo.put("Key", new DBArray());
		try {
			dbo.getObject("Key");
			Assert.fail("Error: Exception should've been reached");
		}
		catch(Exception e) {
			assertEquals("Object does not exist at this Key", e.getMessage());
		}
	}
	
	@Test
	void DBObjectValidGetTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd("Hello");
		dba.insertEnd(2.0d);
		dba.insertEnd(new DBArray());
		assertEquals(true , dba.get(1) instanceof Object);
	}
	
	@Test
	void DBObjectInvalidGetTest() throws Exception {
		DBArray dba = new DBArray();
		dba.insertEnd(2.0f);
		try{
			dba.get(0);
			Assert.fail("Error: Exception was not reached");
		}
		catch(Exception e) {
			assertEquals("Out of bounds", e.getMessage());
		}
	}
	
	@Test
	void DBObjectFromStringTest() throws Exception {
		try {
			DBObject dbo = DBObject.fromString("{\"account\":{\"name\":\"Bill\", \"address\":\"123 main street\", \"phones\":[\"619-594-3535\"], \"balance\": 1234.05}}");
			assertEquals(true, dbo.getObject("account").getArray("phones") instanceof DBArray);
		}
		catch (Exception e) { 
			Assert.fail("Error: String was not converted to DBObject");
		}
	}
	
	@Test
	void DBObjectInvalidFromStringTest() throws Exception {
		try {
			DBObject dbo = DBObject.fromString("[1, \"at\", [[1,2,3],3,4]]");
			Assert.fail("Error: Exception was not reached when converting String to Array");
		}
		catch (Exception e) {
			System.out.println();
			assertEquals("Cannot be converted to JSONObject, invalid String", e.getMessage());
		}
	}
	
	/*
	 * 
	 * Database/Transaction/Command/Snapshot/Cursor/Observer Tests
	 * 
	 */
	
	@Test
	void TransactionBasicTest() throws Exception {
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		DBArray dba = new DBArray();
		DBObject dbo = new DBObject();
		transaction.put("Key1", "String");
		transaction.put("Key2", 1);
		transaction.put("Key3", 3.4d);
		transaction.put("Key4", dba);
		transaction.put("Key5", dbo);
		
		if(!transaction.getString("Key1").equals("String"))
			Assert.fail();
		if(transaction.getInt("Key2") != 1)
			Assert.fail();
		if(transaction.getDouble("Key3") != 3.4d)
			Assert.fail();
		if(!transaction.getArray("Key4").equals(dba))
			Assert.fail();
		if(!transaction.getObject("Key5").equals(dbo))
			Assert.fail();
		
		assertEquals("Pass","Pass");
	}
	
	@Test
	void TransactionRemoveTest() throws Exception {
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		transaction.put("Key1", "String");
		transaction.remove("Key1");
		try {
			transaction.get("Key1");
			Assert.fail("Error: Item was not removed from database");
		}
		catch (Exception e) {
			assertEquals("Key has not been inserted into Database", e.getLocalizedMessage());
		}
	}
	
	@Test
	void ClosedTransactionTest() throws Exception {
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		transaction.abort();
		
		try {
			transaction.put("Key", "Test Message");
			Assert.fail("Error: Transaction not locked when it is supposed to be");
		}
		catch (Exception e) {
			assertEquals("Transaction is no longer active", e.getMessage());
		}
	}
	
	@Test
	void IndependentTransactionsTest() throws Exception {
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction1 = db.createTransaction();
		Transaction transaction2 = db.createTransaction();
		
		transaction1.put("Key1", "TestMessage1");
		transaction2.put("Key2", "TestMessage2");
		transaction1.abort();
		assertEquals("TestMessage2",transaction2.getString("Key2"));
	}
	
	@Test
	void DatabaseRestorationTest() throws Exception {
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		
		transaction.put("Key1", 1);
		transaction.put("Key2", "Hello");
		transaction.put("Key3", 2.1d);
		db.snapshot();
		transaction.abort();
		
		transaction = db.createTransaction();
		
		try{
			transaction.get("Key1");
			Assert.fail("Error: Key should be empty");
		}
		catch (Exception e) {}
		
		try{
			transaction.get("Key2");
			Assert.fail("Error: Key should be empty");
		}
		catch (Exception e) {}
		
		try{
			transaction.get("Key3");
			Assert.fail("Error: Key should be empty");
		}
		catch (Exception e) {}
		
		db.restore();
		
		if(transaction.getInt("Key1") != 1)
			Assert.fail("Error: Database failed to restore");
		if(!transaction.getString("Key2").equals("Hello")) 
			Assert.fail("Error: Database failed to restore");
		if(transaction.getDouble("Key3") != 2.1d) 
			Assert.fail("Error: Database failed to restore");
		
		transaction.put("Key4", "TestMessage1");
		transaction.put("Key5", "TestMessage2");
		transaction.abort();
		
		transaction = db.restore();
		if(!transaction.getString("Key5").equals("TestMessage2"))
			Assert.fail("Error: Database failed to restore");
		if(!transaction.getString("Key4").equals("TestMessage1")) 
			Assert.fail("Error: Database failed to restore");
		
		assertEquals(true,true);
	}
	
	@Test
	void CursorObserverTest() throws Exception{
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		
		Cursor c = (Cursor) new GetCursorCommand(db,"Key1",null).execute();
		IntHistoryObserver observer = new IntHistoryObserver();
		c.addObserver(observer);
		transaction.put("Key1", 1);
		transaction.put("Key2", "Hello");
		transaction.put("Key3", 2.1d);
		transaction.put("Key1",2);
		transaction.put("Key2",3);
		transaction.put("Key1",4);
		
		assertEquals(2, observer.getHistory().get(1).intValue());
	}
	
	@Test
	void InvalidCursorObserverTest() throws Exception{
		FileWriter writer = new FileWriter(DEFAULT_SNAPSHOT_FILE ,false);
		writer.close();
		writer = new FileWriter(DEFAULT_COMMANDS_FILE ,false);
		writer.close();
		
		Database db = new Database();
		Transaction transaction = db.createTransaction();
		
		Cursor c = (Cursor) new GetCursorCommand(db,"Key1",null).execute();
		IntHistoryObserver observer = new IntHistoryObserver();
		c.addObserver(observer);
		transaction.put("Key1", 4);
		try {
			transaction.put("Key1", "Hello");
		}
		catch (Exception e) {
			assertEquals("Integer does not exist at this Key", e.getMessage());
		}
		
	}
	
}
