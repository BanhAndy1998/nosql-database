package cs635prog3;

/*
 * This class is a Validator that checks if an
 * object being added to the Database is a DBObject Object.
 */
public class DBObjectValidator extends Validator{

	public DBObjectValidator(Validator successor) {
		super(successor);
	}

	@Override
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(objectToValidate instanceof DBObject) {
			DBObject convertedObject = (DBObject) objectToValidate;
			for(Object item : convertedObject.getData().values()) {
				if(!headOfChain.validateDataType(item, headOfChain))
					return false;
			}
			return true;
		}
		return super.validateDataType(objectToValidate, headOfChain);
	}
}
