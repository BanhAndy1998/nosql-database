package cs635prog3;

/*
 * This class is a Validator that checks if an
 * object being added to the Database is a Double.
 */
public class DoubleValidator extends Validator {

	public DoubleValidator(Validator successor) {
		super(successor);
	}

	@Override
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(objectToValidate instanceof Double)
			return true;
		return super.validateDataType(objectToValidate, headOfChain);
	}
}
