package cs635prog3;

/*
 * This class calls getInt method from a Database class
 * and returns it.
 */
public class GetIntCommand extends Command {

	public GetIntCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	public Integer execute() throws Exception {
		return db.getInt(key);
	}
	
}
