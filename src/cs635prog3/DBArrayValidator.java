package cs635prog3;

/*
 * This class is a Validator that checks if an
 * object being added to the Database is a DBArray Object.
 */
public class DBArrayValidator extends Validator{

	public DBArrayValidator(Validator successor) {
		super(successor);
	}

	@Override
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(objectToValidate instanceof DBArray) {
			DBArray convertedObject = (DBArray) objectToValidate;
			for(Object item : convertedObject.getData()) {
				if(!headOfChain.validateDataType(item, headOfChain))
					return false;
			}
			return true;
		}
		return super.validateDataType(objectToValidate, headOfChain);
	}
}
