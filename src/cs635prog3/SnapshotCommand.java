package cs635prog3;

/*
 * This class calls the snapshot command from the Database class.
 */
public class SnapshotCommand extends Command{

	public SnapshotCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public Object execute() throws Exception {
		db.snapshot();
		return db;
	}

}
