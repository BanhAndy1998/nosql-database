package cs635prog3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import org.json.JSONObject;

/*
 * This class acts as an interface between a Database and outside classes.
 * Allows for outside classes to access and modify data in the Database.
 */
public class Transaction {
	private Database dbToEdit;
	private Stack<Command> modificationHistory = new Stack<Command>();
	private final Validator VALIDATOR;
	private final File COMMANDS_FILE;
	private boolean isActive;
	
	public Transaction(Database dbToEdit) {
		this.dbToEdit = dbToEdit;
		COMMANDS_FILE = new File("commands.txt");
		VALIDATOR = new IntValidator(new DoubleValidator(new StringValidator(new DBArrayValidator(new DBObjectValidator(null)))));
		isActive = true;
	}
	
	public void put(String key, Object data) throws Exception {
		if(!isActive) 
			throw new Exception("Transaction is no longer active");
		if(VALIDATOR.validateDataType(data, VALIDATOR)) {
			Command toExecute = new PutCommand(dbToEdit, key, data);
			modificationHistory.push(toExecute);
			logToFile(Map.entry(toExecute.getClass().getCanonicalName() + "," + key, data));
			toExecute.execute();
		}
	}
	
	public int getInt(String key) throws Exception {
		if(isActive)
			return new GetIntCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public double getDouble(String key) throws Exception {
		if(isActive)
			return new GetDoubleCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public String getString(String key) throws Exception {
		if(isActive)
			return new GetStringCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public DBArray getArray(String key) throws Exception {
		if(isActive)
			return new GetArrayCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public DBObject getObject(String key) throws Exception {
		if(isActive)
			return new GetObjectCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public Object get(String key) throws Exception{
		if(isActive) 
			return new GetCommand(dbToEdit,key,null).execute();
		throw new Exception("Transaction is no longer active");
	}
	
	public Object remove(String key) throws Exception {
		if(!isActive) 
			throw new Exception("Transaction is no longer active");
		Command toExecute = new RemoveCommand(dbToEdit,key,null);
		modificationHistory.push(toExecute);
		logToFile(Map.entry(toExecute.getClass().getCanonicalName() + "," + key, new Object()));
		return toExecute.execute();
	}
	
	public void commit() throws Exception {
		if(!isActive) 
			throw new Exception("Transaction is no longer active");
		new FileWriter(COMMANDS_FILE, false).close();
		isActive = false;
	}
	
	public void abort() throws Exception {
		if(!isActive) 
			throw new Exception("Transaction is no longer active");
		while(!modificationHistory.isEmpty())
			modificationHistory.pop().undo();
		isActive = false;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	/*
	 * Only called when a method that edits the database is called.
	 * Stores the command, the key, and the data added back into the commands file.
	 */
	private void logToFile(Entry<String, Object> entry) throws IOException {
		JSONObject jsonObject = new JSONObject();
		if(entry.getValue() instanceof DBArray)
			jsonObject.put(entry.getKey(), ((DBArray) entry.getValue()).getData());
		else if(entry.getValue() instanceof DBObject)
			jsonObject.put(entry.getKey(), ((DBObject) entry.getValue()).getData());
		else
			jsonObject.put(entry.getKey(), entry.getValue());
		FileWriter fileWriter = new FileWriter(COMMANDS_FILE, true);
		fileWriter.write(jsonObject.toString() + "\n");
		fileWriter.close();
	}
	
	/*
	 * Retrieves and reruns the commands stored in the commands.txt file through reflection.
	 */
	protected void restoreFromLog(String key, Object value) throws Exception {
		Class<?> commandToCreate = Class.forName(key.substring(0,key.indexOf(',')));
		Constructor<?> retrievedConstructor = commandToCreate.getDeclaredConstructor(Database.class, String.class, Object.class);
		Object retrievedCommand = retrievedConstructor.newInstance(dbToEdit,key.substring(key.indexOf(',') + 1,key.length()),value);
		modificationHistory.add((Command) retrievedCommand);
		((Command) retrievedCommand).execute();
	}
}
