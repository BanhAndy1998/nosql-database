package cs635prog3;

/*
 * Abstract class that serves as a base for the many command classes
 * that allow for other classes to modify or receive data from the database.
 */
public abstract class Command {
	protected Database db;
	protected String key;
	protected Object oldValue;
	protected Object newValue;
	
	public Command(Database db, String key, Object newValue) {
		this.db = db;
		this.key = key;
		this.newValue = newValue;
	}
	
	protected void saveBackup(String key) {
		this.key = key;
		try {
			oldValue = db.get(key);
		} catch (Exception e) {
			oldValue = null;
		}
	}
	
	protected void undo() {
		try {
			//Do not undo if the key was modified by another transaction
			if(!db.get(key).equals(newValue))
				return;
			else {
				if(oldValue == null)
					db.remove(key);
				else
					db.put(key, oldValue);
			}
		} catch (Exception e) {}
    }
    
    public abstract Object execute() throws Exception;
}