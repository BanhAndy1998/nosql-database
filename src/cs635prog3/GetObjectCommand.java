package cs635prog3;

/*
 * This class calls getObject method from a Database class
 * and returns it.
 */
public class GetObjectCommand extends Command{

	public GetObjectCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public DBObject execute() throws Exception {
		return db.getObject(key);
	}
	
}
