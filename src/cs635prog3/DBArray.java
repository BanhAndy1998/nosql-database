package cs635prog3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.*;

/*
 * This class is a data structure that can store 
 * Integers, Doubles, Strings, DBObjects, and other DBArrays
 */

public class DBArray {
	private List<Object> data;
	private final Validator VALIDATOR;

	public DBArray() {	
		data = new ArrayList<Object>();
		this.VALIDATOR = new IntValidator(new DoubleValidator(new StringValidator(new DBArrayValidator(new DBObjectValidator(null)))));
	}
	
	public DBArray(List<Object> data) {
		this.data = data;
		this.VALIDATOR = new IntValidator(new DoubleValidator(new StringValidator(new DBArrayValidator(new DBObjectValidator(null)))));
	}
	
	public void insertEnd(Object data) {
		if(VALIDATOR.validateDataType(data, VALIDATOR))
			this.data.add(data);
	}
	
	public int getInt(int index) throws Exception {
		if((index <= length() - 1 && index >= 0) && data.get(index) instanceof Integer)
			return (int) data.get(index);
		throw new Exception("Integer does not exist at this index or is out of bounds");
	}
	
	public double getDouble(int index) throws Exception {
		if((index <= length() - 1 && index >= 0) && data.get(index) instanceof Double)
			return (double) data.get(index);
		throw new Exception("Double does not exist at this index or is out of bounds");
	}
	
	public String getString(int index) throws Exception {
		if((index <= length() - 1 && index >= 0) && data.get(index) instanceof String)
			return (String) data.get(index);
		throw new Exception("String does not exist at this index or is out of bounds");
	}
	
	public DBArray getArray(int index) throws Exception {
		if((index <= length() - 1 && index >= 0) && data.get(index) instanceof DBArray)
			return (DBArray) data.get(index);
		throw new Exception("Array does not exist at this index or is out of bounds");
	}
	
	public DBObject getObject(int index) throws Exception {
		if((index <= length() - 1 && index >= 0) && data.get(index) instanceof DBObject)
			return (DBObject) data.get(index);
		throw new Exception("Object does not exist at this index or is out of bounds");
	}
	
	public Object get(int index) throws Exception {
		if(index <= length() - 1 && index >= 0) 
			return (Object) data.get(index);
		throw new Exception("Out of bounds");
	}
	
	public int length() {
		return data.size();
	}
		
	public String toString() {
		return data.toString();
	}
	
	public Object remove(int index) {
		try {
			return data.remove(index);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	protected List<Object> getData() {
		return data;
	}
	
	/*
	 * Converts a String in JSON format to a DBArray object
	 */
	public static DBArray fromString(String input) throws Exception{
		JSONArray jsonInput;
		try {
			jsonInput = new JSONArray(input);
		}
		catch (Exception e) {
			throw new Exception("Cannot be converted to JSONArray, invalid String");
		}
		List<Object> listInput = jsonInput.toList();
		for(Object jsonItem : listInput) {
			if(jsonItem instanceof java.util.ArrayList) 
				listInput.set(listInput.indexOf(jsonItem), fromString(jsonItem.toString()));
			else if (jsonItem instanceof java.util.Map) 
				listInput.set(listInput.indexOf(jsonItem), DBObject.fromString(jsonItem.toString()));
		}
		return convertNestedObjects(new DBArray(listInput));
	}
	
	/*
	 * JSON converts Strings with [] to ArrayLists, {} to HashMaps, and doubles to BigDecimals
	 * This methods converts the ArrayLists, HashMaps, and BigDecimals that may be stored
	 * deeper inside of the structure into DBArrays, DBObjects, and Doubles.
	 */
	public static DBArray convertNestedObjects(DBArray data) {
		ArrayList<Object> convertedData = new ArrayList<Object>();
		for(Object retrievedData : data.getData()) {
			if(retrievedData instanceof BigDecimal) {
				convertedData.add(((BigDecimal) retrievedData).doubleValue());
			}
			else if(retrievedData instanceof java.util.ArrayList)
				convertedData.add(DBArray.convertNestedObjects(new DBArray((ArrayList) retrievedData)));
			else if(retrievedData instanceof DBArray) {
				convertedData.add(DBArray.convertNestedObjects((DBArray) retrievedData));
			}
			else if (retrievedData instanceof java.util.HashMap) {
				convertedData.add(DBObject.convertNestedObjects(new DBObject((HashMap) retrievedData)));
			}
			else if (retrievedData instanceof DBObject) {
				convertedData.add(DBObject.convertNestedObjects((DBObject) retrievedData));
			}
			else
				convertedData.add(retrievedData);
		}
		return new DBArray(convertedData);
	}
}
