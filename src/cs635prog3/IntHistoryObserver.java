package cs635prog3;

import java.util.ArrayList;
import java.util.List;

/*
 * This observer takes and stores a history of Integer
 * values stored in a key.
 */
public class IntHistoryObserver implements Observer{

	List<Integer> history;
	
	public IntHistoryObserver() {
		history = new ArrayList<Integer>();
	}
	
	@Override
	public void update(Cursor cursor, String key) throws Exception {
		history.add(cursor.getInt(key));
	}
	
	public List<Integer> getHistory(){
		return history;
	}

	
}
