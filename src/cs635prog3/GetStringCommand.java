package cs635prog3;

/*
 * This class calls getString method from a Database class
 * and returns it.
 */
public class GetStringCommand extends Command {

	public GetStringCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public String execute() throws Exception {
		return db.getString(key);
	}

}
