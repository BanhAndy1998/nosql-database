package cs635prog3;

/*
 * Interface for Observers to be used n the Database class.
 */
public interface Observer {
	public void update(Cursor cursor, String key) throws Exception;
}
