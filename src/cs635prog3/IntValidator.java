package cs635prog3;

/*
 * This class is a Validator that checks if an
 * object being added to the Database is an Integer Object.
 */
public class IntValidator extends Validator{

	public IntValidator(Validator successor) {
		super(successor);
	}

	@Override
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(objectToValidate instanceof Integer)
			return true;
		else
			return super.validateDataType(objectToValidate, headOfChain);
	}
}
