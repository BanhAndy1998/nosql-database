package cs635prog3;

/*
 * This class is a Validator that checks if an
 * object being added to the Database is a String Object.
 */
public class StringValidator extends Validator{

	public StringValidator(Validator successor) {
		super(successor);
	}

	@Override
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(objectToValidate instanceof String)
			return true;
		return super.validateDataType(objectToValidate, headOfChain);

	}
}
