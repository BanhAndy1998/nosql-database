package cs635prog3;

/*
 * This class calls the getCursor method from a Database class
 * and returns it.
 */
public class GetCursorCommand extends Command{
	public GetCursorCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public Object execute() throws Exception {
		return db.getCursor(key);
	}
	
}
