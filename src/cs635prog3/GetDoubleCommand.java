package cs635prog3;

/*
 * This class calls the getDouble method from a Database class
 * and returns it.
 */
public class GetDoubleCommand extends Command{

	public GetDoubleCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public Double execute() throws Exception {
		return db.getDouble(key);
	}

}
