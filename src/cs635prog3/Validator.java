package cs635prog3;

/*
 * Abstract class that is used as a base for other Validator classes.
 * For use in validating that objects that are being inserted into
 * the Database are of the correct data type.
 */
public abstract class Validator {
	private Validator successor;
	
	public Validator(Validator successor) {
		this.successor = successor;
	}
	
	public boolean validateDataType(Object objectToValidate, Validator headOfChain) {
		if(successor != null) {
			return successor.validateDataType(objectToValidate, headOfChain);
		}
		return false;
	}
}
