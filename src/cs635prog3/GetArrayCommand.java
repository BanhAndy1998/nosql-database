package cs635prog3;

/*
 * This class calls the getArray method from a Database class
 */
public class GetArrayCommand extends Command {

	public GetArrayCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public DBArray execute() throws Exception {
		return db.getArray(key);
	}

}
