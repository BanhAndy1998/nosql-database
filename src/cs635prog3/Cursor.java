package cs635prog3;

import java.util.ArrayList;
import java.util.List;

/*
 * This class peeks at a key inside of a Database class and notifies
 * any Observer classes stored in this class when the value inside of
 * that key is changed.
 */
public class Cursor {
	String key;
	Database currentDB;
	List<Observer> observers = new ArrayList<Observer>();
	
	public Cursor(String key, Database db) {
		this.key = key;
		this.currentDB = db;
	}
	
	public int getInt(String key) throws Exception {
		return new GetIntCommand(currentDB,key,null).execute();
	}
	
	public double getDouble(String key) throws Exception {
		return new GetDoubleCommand(currentDB,key,null).execute();
	}
	
	public String getString(String key) throws Exception {
		return new GetStringCommand(currentDB,key,null).execute();
	}
	
	public DBArray getArray(String key) throws Exception {
		return new GetArrayCommand(currentDB,key,null).execute();
	}
	
	public DBObject getObject(String key) throws Exception {
		return new GetObjectCommand(currentDB,key,null).execute();
	}
	
	public Object get(String key) throws Exception{
		return new GetCommand(currentDB,key,null).execute();
	}
	
	public void addObserver(Observer observer) {
		observers.add(observer);
	}
	
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}
	
	public void update() throws Exception {
		for (Observer toUpdate : observers) {
			toUpdate.update(this, key);
		}
	}
}
