package cs635prog3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

/*
 * This class is a data structure that can store 
 * Integers, Doubles, Strings, DBArrays, and other DBObjects
 */
public class DBObject {
	private Map<String,Object> data; 
	private final Validator VALIDATOR;
	
	public DBObject() {
		data = new HashMap<String,Object>();
		this.VALIDATOR = new IntValidator(new DoubleValidator(new StringValidator(new DBArrayValidator(new DBObjectValidator(null)))));
	}
	
	public DBObject (Map<String,Object> data) {
		this.data = data;
		this.VALIDATOR = new IntValidator(new DoubleValidator(new StringValidator(new DBArrayValidator(new DBObjectValidator(null)))));
	}
	
	public DBObject put(String key, Object data) {
		if(VALIDATOR.validateDataType(data, VALIDATOR))
			this.data.put(key, data);
		return this;
	}
	
	public int getInt(String key) throws Exception {
		if(data.get(key) instanceof Integer)
			return (int) data.get(key);
		throw new Exception("Integer does not exist at this Key");
	}
	
	public double getDouble(String key) throws Exception {
		if(data.get(key) instanceof Double)
			return (double) data.get(key);
		throw new Exception("Double does not exist at this Key");
	}
	
	public String getString(String key) throws Exception {
		if(data.get(key) instanceof String)
			return (String) data.get(key);
		throw new Exception("String does not exist at this Key");
	}
	
	public DBArray getArray(String key) throws Exception {
		if(data.get(key) instanceof DBArray)
			return (DBArray) data.get(key);
		throw new Exception("Array does not exist at this Key");
	}
	
	public DBObject getObject(String key) throws Exception {
		if(data.get(key) instanceof DBObject)
			return (DBObject) data.get(key); 
		throw new Exception("Object does not exist at this Key");
	}
	
	public Object get(String key) throws Exception{
		if(data.containsKey(key))
			return (Object) data.get(key); 
		throw new Exception("Key has not been inserted into this Object");
	}
	
	public void length(){
		data.size();
	}
	
	public Object remove(String key){
		try {
			return data.remove(key);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public String toString(){
		return data.toString();
	}
	
	protected Map<String, Object> getData() {
		return data;
	}
	
	/*
	 * Converts a String in JSON format to a DBObject object
	 */
	public static DBObject fromString(String input) throws Exception {
		JSONObject jsonInput;
		try {
			 jsonInput = new JSONObject(input);
		}
		catch (Exception e) {
			throw new Exception("Cannot be converted to JSONObject, invalid String");
		}
		Map<String, Object> mapInput = jsonInput.toMap();
		for(Entry<String, Object> jsonItem : mapInput.entrySet()) {
			if(jsonItem.getValue() instanceof java.util.ArrayList) 
				mapInput.replace(jsonItem.getKey(), DBArray.fromString(jsonItem.getValue().toString()));
			else if (jsonItem.getValue() instanceof java.util.HashMap) 
				mapInput.replace(jsonItem.getKey(), new DBObject((Map<String,Object>) jsonItem.getValue()));
		}
		return convertNestedObjects(new DBObject(mapInput));
	}
	
	/*
	 * This methods converts the ArrayLists, HashMaps, and BigDecimals that may be stored
	 * deeper inside of the structure into DBArrays, DBObjects, and Doubles. As JSON converts 
	 * Strings with [] to ArrayLists, {} to HashMaps, and doubles to BigDecimals
	 */
	public static DBObject convertNestedObjects(DBObject data) {
		Map<String,Object> convertedData = new HashMap<String,Object>();
		for(Entry<String,Object> retrievedData : data.getData().entrySet()) {
			if(retrievedData instanceof BigDecimal) {
				convertedData.put(retrievedData.getKey(), ((BigDecimal) retrievedData).doubleValue());
			} if(retrievedData.getValue() instanceof java.util.ArrayList)
				convertedData.put(retrievedData.getKey(), DBArray.convertNestedObjects(new DBArray((ArrayList) retrievedData.getValue())));
			else if(retrievedData.getValue() instanceof DBArray) {
				convertedData.put(retrievedData.getKey(), DBArray.convertNestedObjects((DBArray) retrievedData.getValue()));
			}
			else if (retrievedData.getValue() instanceof java.util.HashMap) {
				convertedData.put(retrievedData.getKey(), DBObject.convertNestedObjects(new DBObject((HashMap) retrievedData.getValue())));
			}
			else if (retrievedData.getValue() instanceof DBObject) {
				convertedData.put(retrievedData.getKey(), DBObject.convertNestedObjects((DBObject) retrievedData.getValue()));
			}
			else
				convertedData.put(retrievedData.getKey(), retrievedData.getValue());
		}
		return new DBObject(convertedData);
	}
}
