package cs635prog3;

/*
 * This class calls the put method from a Database class
 * and inserts the value into the database if it valid
 */

public class PutCommand extends Command {

	public PutCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	@Override
	public Object execute() throws Exception {
		super.saveBackup(key);
		db.put(key, newValue);
		return newValue;
	}
	
}
