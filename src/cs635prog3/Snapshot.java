package cs635prog3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.json.*;

/*
 * This class handles archiving the Database into a text file
 * for archiving and restoration.
 */

public class Snapshot {
	private final File SNAPSHOT_FILE;
	
	public Snapshot(Database database, Map<String, Object> data, File commands, File snapshot) throws IOException {
		SNAPSHOT_FILE = snapshot;
		JSONObject object = new JSONObject();
		for(Entry<String, Object> entry : data.entrySet()) {
			if(entry.getValue() instanceof DBArray)
				object.put(entry.getKey(), ((DBArray) entry.getValue()).getData());
			else if(entry.getValue() instanceof DBObject)
				object.put(entry.getKey(), ((DBObject) entry.getValue()).getData());
			else
				object.put(entry.getKey(), entry.getValue());
		}
		FileWriter writer = new FileWriter(SNAPSHOT_FILE);
		writer.write(object.toString());
		writer.close();
		//Clear the commands file
		writer = new FileWriter(commands,false);
		writer.close();
	}

	
}
