package cs635prog3;

/*
 * This class calls the get method from a Database class
 * and returns it.
 */
public class GetCommand extends Command{
	
	public GetCommand(Database db, String key, Object newValue) {
		super(db, key, newValue);
	}

	public Object execute() throws Exception {
		return db.get(key);
	}
	
	
}
